<?php
/**
 * Created by Tom Berning.
 * Aloha Heja Heja...
 */
class Backend
{

    public function fetchAll(mysqli $mysqli)
    {
        $mysqli -> set_charset('utf8');
        $result = $mysqli -> query('SELECT `BfName` FROM `DBBfs`');

        $data = array();
        while($row = $result -> fetch_assoc()){
            $data[] = $row;
        }

        $mysqli -> close();

        return json_encode($data);

    }

    public function fetchSpecific(mysqli $mysqli, $station)
    {

        $result = $mysqli -> query("SELECT * FROM `SEARCHINDEX_BAK` WHERE BfName LIKE " . "'$station'");

        $data = array();
        while($row = $result -> fetch_assoc()){
            $data[] = $row;
        }

        $mysqli -> close();

        return json_encode($data);

    }

}

