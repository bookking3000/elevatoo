<!DOCTYPE html>


<html>

<head>
    <meta charset="UTF-8">
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/StyleSheet.css">


    <title>Aufzugs/Rolltreppen - Abfrage </title>
</head>

<body>

<div class="form">

    <?php include_once("analyticstracking.php");

    ?>

    <h1> Aufz&uuml;ge und Rolltreppen der DB Station&Service AG </h1>

    <div class="row">
        <form method="post" action="">
            <div class="row">
                <div id="search_query">

                    <input type="text" name="StationID" placeholder="ID(Zahl) "/>

                    <input type="text" name="StationIDn" placeholder="Bahnhofsname "/>

                    <input type="text" name="DS100" placeholder="DS100 "/>

                    <input type="submit" name="submit" value="Suchen...">

                </div>
            </div>

        </form>
    </div>
</div>


    <div class="datagrids">
        <?php
        header('Content-Type: text/html; charset=ISO-8859-1');

        //
        // Database `DeutscheBahn` Indexes for Search
        //

        $WGS84Area = "6.218262%2C47.070122%2C15.88623%2C54.749991";
        $host = "localhost";
        $username = "Henry";
        $password = "W00lverin3ab§";
        $dbname = "DeutscheBahn";
        $con = mysqli_connect($host, $username, $password, $dbname) or die('Error in Connecting: ' . mysqli_error($con) . "" . mysqli_errno($con));
        $link = $con;




        $checktable = mysqli_query($link, "SHOW TABLES LIKE '$table'");
        $table_exists = mysqli_num_rows($checktable) > 0;

        if ($table_exists) {

            $table = "SEARCHINDEX";

        } else {

            $table = "SEARCHINDEX_BAK";

        }

        if ($_POST['StationIDn']) {

            $query = $_POST['StationIDn'];
            $sql = "SELECT * FROM $table WHERE BfName LIKE " . "'%$query%'";
        } elseif ($_POST['StationID']) {

            $query = $_POST['StationID'];
            $sql = "SELECT * FROM $table WHERE Bfnr LIKE " . "$query";
        } elseif ($_POST['DS100']) {

            $query = $_POST['DS100'];
            $sql = "SELECT * FROM $table WHERE DS100 LIKE " . "'%$query%'";
        } else {

            echo "<p> Please Enter Something <p>"; //Dont show whole DB
            //JOIN tbl_DB_F ON DBBfs .Bfnr = tbl_DB_F .Bfid";
        }

        if ($result = mysqli_query($link, $sql)) {
            if (mysqli_num_rows($result) > 0) {


                echo '<table class="striped"> ';
                echo "<tr>";
                echo "<th>Bfnr</th>";
                echo "<th>Bahnhofsname</th>";
                echo "<th>DS100</th>";
                echo "<th>PLZ</th>";
                echo "<th>ORT</th>";
                echo "<th>TYPE</th>";
                echo "<th>STATUS</th>";
                echo "<th>ID</th>";
                echo "<th>Beschreibung</th>";
                echo "</tr>";

                while ($row = mysqli_fetch_array($result)) {
                    echo "<tr>";
                    echo "<td>" . $row['Bfnr'] . "</td>";
                    echo "<td>" . $row['BfName'] . "</td>";
                    echo "<td>" . $row['DS100'] . "</td>";
                    echo "<td>" . $row['PLZ'] . "</td>";
                    echo "<td>" . $row['ORT'] . "</td>";

                    if ($row['type'] == "ESCALATOR") {
                        echo '<td> Rolltreppe </td>';
                    } else if ($row['type'] == "ELEVATOR") {
                        echo '<td> Aufzug </td>';
                    } else {
                        echo '<td>N.A!</td>';
                    }

                    if ($row['state'] == "INACTIVE") {
                        echo '<td style="background-color:red;">' . $row['state'] . "</td> ";
                    } else {
                        echo "<td>" . $row['state'] . "</td>       ";
                    }

                    echo "<td>" . $row['id'] . "</td>";
                    echo "<td>" . $row['desc'] . "</td>";
                    echo "</tr>";
                }

                echo "</table>";
                echo "</div>";

                mysqli_free_result($result);
            } else {
                echo "No records matching this Station.";
            }
        } else {

        }

        mysqli_close($link);

        echo "<hr>";
        ?>
    </div>

<footer>
    <a> (c) 2017 Tom Berning </a>
</footer>

</body>
</html>
