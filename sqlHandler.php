<?php
require_once('/var/www/html/Elevatoo/Config.php');

class sqlHandler
{

    public $mysqli;

    /**
     * @return mixed
     */
    public function getMysqli()
    {
        return $this -> mysqli;
    }

    /**
     * @param mixed $mysqli
     */
    public function setMysqli($mysqli)
    {
        $this -> mysqli = $mysqli;
    }

    public function connectToDeutscheBahn(){
        $config = new Config();
        $mysqli = new mysqli();
        $mysqli -> connect($config -> getDBUrl(), $config -> getUser(), $config -> getPassword(), $config -> getDB());
    }



}