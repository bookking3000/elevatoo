<?php

class Util
{
    public function tableExists(mysqli $mysqli, $table)
    {
        $query = $mysqli -> query("SHOW TABLES LIKE '$table'");
        return $table_exists = mysqli_num_rows($query) > 0;
    }

    public function openIframe($type)
    {

        if ($type == "exec")
        {
            return "<iframe src='./data.php?method=all' >" + PHP_EOL;
        }
        else if ($type == "exec_s")
        {
            return "<iframe src='./data.php?method=station&name=Siegburg/Bonn' >";
        }
        else if ($type != null)
        {
            return "Error: Wrong Method Type";
        }

    }

}