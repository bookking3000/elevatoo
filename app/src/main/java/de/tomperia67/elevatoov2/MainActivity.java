package de.tomperia67.elevatoov2;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.crash.FirebaseCrash;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.HashMap;
import java.util.List;

public class MainActivity extends AppCompatActivity implements OnMapReadyCallback {
    private FirebaseAnalytics mFirebaseAnalytics;
    private GoogleMap mMap;
    private LatLng mLatLng;
    boolean HashSaved;
    CustomAutoCompleteTextView mEditText;
    ListView mResponse;
    JSONArray mJsonArray;
    List<String> shared;
    HashMap<String, String> MapData;
    Context mC = this;
    public static double geoY;
    public static double geoX;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mEditText = (CustomAutoCompleteTextView) findViewById(R.id.editText);
        mResponse = (ListView) findViewById(R.id.textView);

        int SDK_INT = android.os.Build.VERSION.SDK_INT;
        if (SDK_INT > 8) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        new DataGetter(mResponse, mEditText.getText().toString(), mJsonArray, shared, this, mEditText, this).execute("all");

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

    }

    public void callBackend(View view) throws JSONException {

        FirebaseCrash.report(new Exception("Calling Backend"));

        new DataGetter(mResponse, mEditText.getText().toString(), mJsonArray, shared, this, mEditText, this).execute("specific");
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, "Backend-Call");
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "SpecificFacilityRequest");
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "text");
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);



    }

    public void showDialog() {

        final Dialog customDialog = new Dialog(this);

        View dView = LayoutInflater.from(MainActivity.this).inflate(R.layout.activity_bf_details, null);

        TextView geoXtv = (TextView) dView.findViewById(R.id.geoX);
        TextView geoYtv = (TextView) dView.findViewById(R.id.geoY);
        customDialog.show();

    }

    public static double getGeoX() {
        return geoX;
    }

    public static double getGeoY() {
        return geoY;
    }

    public void saveHashmap(HashMap<String, String> stringHashMap) {

        MapData = stringHashMap;
        System.out.println("MA.d: " + MapData.toString());
        geoX = Double.parseDouble(stringHashMap.get("geocoordX"));
        geoY = Double.parseDouble(stringHashMap.get("geocoordY"));
        HashSaved = true;
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        //mMap.setPadding(100,100,100,100);
        System.out.println("googleMap = [" + googleMap + "]");

        if (HashSaved) {
            mLatLng = new LatLng(MainActivity.getGeoX(), MainActivity.getGeoY());
            System.out.println("LAT_LNG");
            mMap.addMarker(new MarkerOptions().position(mLatLng).title("Bahnhof!"));
            mMap.moveCamera(CameraUpdateFactory.newLatLng(mLatLng));
        } else {
            mLatLng = new LatLng(2, 2);
        }


    }

    //C:\Users\tompe\AppData\Local\Android\sdk\platform-tools
}

