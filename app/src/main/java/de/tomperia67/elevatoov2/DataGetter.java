package de.tomperia67.elevatoov2;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class DataGetter extends AsyncTask<String, Void, String> {

    private URLConnection conn;
    private ListView listView;
    private String station;
    private JSONArray mJsonArray;
    private URL url;
    private String mParams;
    private List<String> ac1;
    private Context mContext;
    private CustomAutoCompleteTextView autoCompleteTextView;
    ProgressDialog pDialog;
    private MainActivity MAD;

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        pDialog = new ProgressDialog(mContext);
        pDialog.setMessage("Please wait...");
        pDialog.setCancelable(false);
        pDialog.show();

    }


    @Override
    protected String doInBackground(String... params) {

        mParams = params[0];

        try {

            switch (mParams) {
                case "all":
                    url = new URL("http://www.t-j-e.ml/Elevatoo/backend/data.php?method=all");
                    openConnection(url);
                    break;

                case "specific":
                    url = new URL("http://www.t-j-e.ml/Elevatoo/backend/data.php?method=station&StationName=" + station);
                    SO("Connecting to: "+ url.toString());
                    openConnection(url);
                    break;

                default:
                    SO("Invalid Arguments");
                    break;
            }


            System.out.println("Reading Data from Backend!");
            return readResult();

        } catch (IOException e) {

            e.printStackTrace();

        }

        System.out.println("Getting Data from Backend has failed ");
        return null;

    }

    DataGetter(ListView listView, String a, JSONArray b, List<String> c, Context d, CustomAutoCompleteTextView e,MainActivity f) {
        this.listView = listView;
        this.station = a;
        this.mJsonArray = b;
        this.ac1 = c;
        this.mContext = d;
        this.autoCompleteTextView = e;
        this.MAD = f;
    }

    /**
     * Opens the Connection with the Backend
     */
    private void openConnection(URL url) throws IOException {

        //Backend-URL:
        conn = url.openConnection();
        conn.setDoOutput(true);

    }

    private void SO(String x) {
        System.out.println(x);
    }


    /**
     * Reads from the URLConnection!
     */
    private String readResult() throws IOException {

        BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        StringBuilder sb = new StringBuilder();
        String line;

        while ((line = reader.readLine()) != null) {

            sb.append(line);

        }

        try {

            mJsonArray = new JSONArray(sb.toString());

        } catch (JSONException e) {

            e.printStackTrace();
        }

        return sb.toString();
    }


    @Override
    protected void onPostExecute(String result) {
        if (!isBlank(result)) {

            try {
                switch (mParams) {

                    case "specific":
                        int[] flags = new int[]{
                                R.drawable.elevator,
                                R.drawable.elevator_dis,

                        };

                        ArrayList<HashMap<String, String>> bfs = new ArrayList();
                        JSONArray jsonArray = mJsonArray;

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject c = jsonArray.getJSONObject(i);

                            String desc = c.getString("desc");
                            String type = c.getString("type");
                            String state = c.getString("state");
                            String geocoordX = c.getString("geocoordX");
                            String geocoordY = c.getString("geocoordY");

                            HashMap<String, String> stringHashMap = new HashMap<>();

                            if (type.equals("ELEVATOR")) {
                                stringHashMap.put("type", "Aufzug");
                            } else if (type.equals("ESCALATOR")) {
                                stringHashMap.put("type", "Aufzug");
                            }

                            stringHashMap.put("desc", desc);
                            stringHashMap.put("geocoordX", geocoordX);
                            stringHashMap.put("geocoordY", geocoordY);
                            stringHashMap.put("state", state);

                            if (state.equals("ACTIVE")) {
                                stringHashMap.put("icon_x", Integer.toString(flags[0]));
                            } else {
                                stringHashMap.put("icon_x", Integer.toString(flags[1]));
                            }

                            bfs.add(stringHashMap);

                            SO(stringHashMap.toString());
                            MAD.saveHashmap(stringHashMap);

                        }

                        ListAdapter adapter = new SimpleAdapter(mContext, bfs, R.layout.list_item_bfs,
                                new String[]{"type", "desc", "icon_x",}, new int[]{R.id.firstLine,
                                R.id.secondLine, R.id.icon_x});

                        listView.setAdapter(adapter);
                        listView.setClickable(true);
                        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                                SO(listView.getItemAtPosition(position).toString());
                                MAD.showDialog();

                            }


                        });
                        pDialog.dismiss();

                        break;

                    case "all":
                        ac1 = computeAll(new JSONArray(result));
                        ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(mContext, android.R.layout.simple_dropdown_item_1line, ac1);
                        autoCompleteTextView.setAdapter(adapter2);
                        pDialog.dismiss();
                        break;

                    default:
                        break;
                }


            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private boolean isBlank(String value) {

        return value == null || value.trim().isEmpty();

    }

    private List<String> computeSpecific(JSONArray mJsonArray) throws Exception {

        List<String> list = new ArrayList<String>();
        for (int i = 0; i < mJsonArray.length(); i++) {
            list.add(mJsonArray.getJSONObject(i).getString("type"));
            list.add(mJsonArray.getJSONObject(i).getString("desc"));
            list.add(mJsonArray.getJSONObject(i).getString("state"));
        }

        return list;

    }

    private List<String> computeAll(JSONArray mJsonArray2) throws Exception {

        List<String> list = new ArrayList<String>();

        for (int i = 0; i < mJsonArray2.length(); i++) {
            list.add(mJsonArray2.getJSONObject(i).getString("BfName"));
        }

        return list;
    }


}
