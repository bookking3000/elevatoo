<?php

/**
 * Created by PhpStorm.
 * User: tompe
 * Date: 22.09.2017
 * Time: 18:58
 */
class Config
{
    //API Things
    public $API_Url;


    //MySQL Things
    public $DB_Url;
    public $User;
    public $Password;
    public $DB;

    /**
     * @return mixed
     */
    public function getDB()
    {
        return $this -> DB;
    }

    /**
     * @param mixed $DB
     */
    public function setDB($DB)
    {
        $this -> DB = $DB;
    }

    function __construct()
    {
        $this -> setUser("Henry");
        $this -> setAPIUrl("https://api.deutschebahn.com/fasta/v1/facilities?area=5.218262%2C47.070122%2C15.88623%2C54.749991");
        $this -> setDBUrl("localhost");
        $this -> setPassword("W00lverin3ab§");
        $this -> setDB("DeutscheBahn");
    }

    /**
     * @return mixed
     */
    public function getAPIUrl()
    {
        return $this -> API_Url;
    }

    /**
     * @param mixed $API_Url
     */
    public function setAPIUrl($API_Url)
    {
        $this -> API_Url = $API_Url;
    }

    /**
     * @return mixed
     */
    public function getDBUrl()
    {
        return $this -> DB_Url;
    }

    /**
     * @param mixed $DB_Url
     */
    public function setDBUrl($DB_Url)
    {
        $this -> DB_Url = $DB_Url;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this -> User;
    }

    /**
     * @param mixed $User
     */
    public function setUser($User)
    {
        $this -> User = $User;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this -> Password;
    }

    /**
     * @param mixed $Password
     */
    public function setPassword($Password)
    {
        $this -> Password = $Password;
    }


}