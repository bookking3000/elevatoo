<!DOCType html>
<html>
<head>
    <title>Elevatoo -Backend</title>
    <meta charset="utf-8">
</head>
<body>

<?php

require('../Config.php');
require('../JSONImporter.php');
require('../Backend.php');
require('../Util.php');

$authorization = "Authorization: Bearer bf25b08646cae51198b0328a928b0b95";

$config = new Config();
$backend = new Backend();
$mysqli = new mysqli();
$util = new Util();

$mysqli -> connect($config -> getDBUrl(), $config -> getUser(), $config -> getPassword(), $config -> getDB());

$method = $_GET['method'];

if ($method == "all")
{
    $data = $backend -> fetchAll($mysqli);
    print_r ($data);
}
else if ($method == "station")
{
    $station = $_GET['name'];
    $data = $backend -> fetchSpecific($mysqli, $station);
    print_r ($data);
}
else
{

    echo "<div id='BackendInfo'>";

    echo "Welcomme to ElevatooBackend V2.0.1.8  <br> <br>";
    echo "Get all Train-Stations in Germany:    <br>";
    echo "<a href='display.php?method=exec'> Klick! </a> <br> <br>";

    echo "Get the Current States in Siegburg/Bonn: <br>";
    echo "<a href='display.php?method=exec_s'> Klick! </a> <br> <br>";

    echo "</div>";

    echo "<div id='BackendData'>";

    echo $util -> openIframe($method);

    echo "</div>";

}

?>

</body>
</html>





