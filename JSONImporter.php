<?php

class JSONImporter
{
    /**
     * Gets JSON-Array from the API-Server
     * @param $url string Of the API-Server
     * @param $authkey string Authentication-Token
     * @return mixed JSON-Array
     */

    public function getJSON($url, $authkey)
    {
        $curl = curl_init();

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "GET");

        $headers = array();
        $headers[] = $authkey;
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($curl);

        curl_close($curl);
        return json_decode($result, true);
    }

    public function insertIntoDB(mysqli $mysqli, array $data)
    {
        $mysqli -> query('TRUNCATE TABLE tbl_DB_F' && 'DROP TABLE SEARCHINDEX_BAK' && 'CREATE TABLE SEARCHINDEX_BAK SELECT * FROM SEARCHINDEX' && 'DROP TABLE SEARCHINDEX');

        foreach ($data as $row) {
            if ($stmt = $mysqli -> prepare("INSERT INTO tbl_DB_F(`id`, `Bfid`, `desc`, `type`, `state`, `geocoordX`,`geocoordY`) VALUES (?, ?, ?, ?, ?,?,?)")) {
                $stmt -> bind_param('iisssss', $row["equipmentnumber"], $row["stationnumber"], $row["description"], $row["type"], $row["state"], $row["geocoordX"], $row["geocoordY"]);
                $stmt -> execute();
            }
            else
            {
                echo "Fail"; echo $mysqli -> error;
            }
        }

        $mysqli -> query('CREATE TABLE SEARCHINDEX SELECT * FROM DBBfs LEFT JOIN tbl_DB_F ON DBBfs .Bfnr = tbl_DB_F .Bfid');
        $mysqli -> close();
    }
}