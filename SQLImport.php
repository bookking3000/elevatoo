<?php
/**  ELEVATOO
 *
 * This project was made possible thanks to the APIs and Datasets published by
 * the DB Station&Service AG on the DB OpenData-Portal (BETA) as well as DB-Developers Portal.
 *
 * The Facility data is provided by the FaSta API, licensed under CC BY 4.0.
 *
 * DB Station&Service AG overtakes no responsibility and gives no guarantee for
 * the completeness, correctness or accuracy of the data.
 *
 * (c) 2017 - Tom Berning
 */

    require('Config.php');
    require('JSONImporter.php');

    echo "Importing....<br> Please Wait <br> <br>";

    $authorization = "Authorization: Bearer bf25b08646cae51198b0328a928b0b95";
    $config = new Config();
    $importer = new JSONImporter();

    $jsonData = $importer -> getJSON($config -> getAPIUrl(), $authorization);
    print_r($jsonData);

    $mysqli = new mysqli();
    $mysqli -> connect($config -> getDBUrl(), $config -> getUser(), $config -> getPassword(), $config -> getDB());

    $importer -> insertIntoDB($mysqli, $jsonData);

